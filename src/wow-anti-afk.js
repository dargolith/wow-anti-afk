#!/usr/bin/env node

import { mouse, right, left, Button } from '@nut-tree-fork/nut-js';

const defaultInterval = 300; // 300 seconds

const moveForward = async (timer = 0.1) => {
  await mouse.move(right(300));
  await mouse.move(left(300));
  await mouse.pressButton(Button.LEFT);
  await mouse.pressButton(Button.RIGHT);
  await new Promise(resolve => setTimeout(resolve, timer * 1000));
  await mouse.releaseButton(Button.LEFT);
  await mouse.releaseButton(Button.RIGHT);
};

export const start = args => {
  const interval = args[2] || defaultInterval;
  console.log('==================================================================');
  console.log('                    World of Warcraft Anti-AFK                    ');
  console.log();
  console.log(`Running with update interval: ${interval} seconds.`);
  console.log('Make sure your cursor is in your World of Warcraft application.');
  console.log('==================================================================');
  setInterval(moveForward, interval * 1000);
};

if (require.main === module) {
  start();
}
