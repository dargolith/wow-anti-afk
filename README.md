# World of Warcraft Anti AFK

Simple script for keeping your character from going AFK in World of Warcraft.

## Installation

`npm install wow-anti-afk`


## Run

`npm start wow-anti-afk`